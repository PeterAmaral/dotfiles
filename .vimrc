" Peter Amaral's .vimrc

" Don't need vi compatibilty
set nocompatible
set t_Co=256 "Pretty colors

" automatic syntax and indent detection based on file extension
filetype plugin indent on
colorscheme desert
syntax on

" Color settings
set background=dark
highlight Normal     guifg=gray guibg=black

" Nice status line
set statusline=\ %F\ %m\ %r%h%w[%{&ff}]\ %y\ [%l,%v]\ [%p%%]\ %{strftime(\"%c\")}
" %{strftime(\"%m/%d/%y\ -\ %H:%M\")}
set laststatus=2
hi StatusLine ctermbg=cyan ctermfg=darkblue

" Line numbers
set number

" 80 lines wide marker
set tw=80
set colorcolumn=80

" Indents are two spaces
set sw=2

" Spell check
au BufRead *.txt setlocal spell

" Better search
set ignorecase
set smartcase

" Better tab complete
set wildmode=longest,list,full
set wildmenu

" Treat WSDL files as XML
au BufNewFile,BufRead *.wsdl set filetype=xml

" Syntax highlighting for log files
au BufNewFile,BufRead *.log,*.out,*.diagnostic-log set filetype=log4j

" Highlight matching brackets, including angles
set showmatch
set matchpairs+=<:>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Netrw settings
let g:netrw_liststyle=3
let g:netrw_browse_split=2
let g:netrw_winsize=25

"call plug#begin()
"Plug 'scrooloose/nerdtree', { 'on': 'NerdTreeToggle' }
"call plug#end()